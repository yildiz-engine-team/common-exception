package be.yildizgames.common.exception.business;

/**
 * High level exception meant to be sent to the user, this is the exception
 * dealing with all user misbehavior like trying to reach an non-existing resource...
 * This exception is thrown because of user misbehavior, not system failure.
 * Immutable class.
 *
 * @author Grégory Van den Borre
 */
public class BusinessException extends RuntimeException {

    protected BusinessException(String message) {
        super(message);
    }

    protected BusinessException(Exception cause) {
        super(cause);
    }

    protected BusinessException(String message, Exception cause) {
        super(message, cause);
    }
}
